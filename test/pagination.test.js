import Pagination from '../component/Pagination'

test('Pagination constructs with total pages', () => {
  let testPagination = new Pagination(123)
  expect(testPagination).toEqual({totalPages: 123});
});

test('Pagination test if no records', () => {
  let testPagination = new Pagination(0)
  let html = testPagination.render();
  expect(html).toEqual('<div class="row"><div class="d-flex center"><h3>No records</h3></div></div>');
});