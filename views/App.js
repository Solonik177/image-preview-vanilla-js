import axios from 'axios'
import Header from './Header'
import Pagination from '../component/Pagination'
import Modal from '../component/Modal'

export default class {
  constructor(rootElement) {
    this.rootElement = rootElement
    this.images = []
    this.page = 1
    this.limit = 12
    this.total = 100
    this.header = new Header()
    this.modal = new Modal()
    this.pagination = new Pagination(this.totalPages())
    this.fetchImages()
    this.initialize()
  }

  render() {
    let limit = parseInt(this.limit)

    this.rootElement.innerHTML = `${this.header.render()}
        <div class="container">
          <div class="row">
            <div class="col-sm-12 d-flex right">
               <span class="mr-3">Rows per page:</span>
               <select class="rows-per-page">
                <option value="12" ${limit === 12 ? 'selected' : ''}>12</option>
                <option value="20" ${limit === 20 ? 'selected' : ''}>20</option>
                <option value="25" ${limit === 25 ? 'selected' : ''}>25</option>
                <option value="30" ${limit === 30 ? 'selected' : ''}>30</option>
              </select>
            </div>
          </div>
          <div class="row">
            ${this.images
              .map(function(image) {
                return `<div class="col-sm-12 col-md-4 col-lg-3 d-flex center"><img class="responsive-image" src="${image.download_url}" alt="${image.author}" /></div>`
              })
              .join('')}  
          </div>         
          ${this.pagination.render(this.page)}  
          ${this.modal.render()}      
        </div>`
  }

  initialize() {
    document.querySelector('body').addEventListener('click', (event) => {
      if (event.target.className.toLowerCase().trim() === 'page-item') {
        this.handlePage(event.target.dataset.page)
      } else if (
        event.target.className.toLowerCase().trim() === 'responsive-image'
      ) {
        document.getElementById('imagePreview').src = event.target.src
        document.querySelector('.modal').classList.add('active')
      } else if (
        event.target.className.toLowerCase().trim() === 'close-modal'
      ) {
        document.getElementById('imagePreview').src = '#'
        document.querySelector('.modal').classList.remove('active')
      }
    })

    document.querySelector('body').addEventListener('change', (event) => {
      if (event.target.className.toLowerCase() === 'rows-per-page') {
        this.handleLimit(event.target.value)
      }
    })
  }

  totalPages() {
    return Math.ceil(this.total / this.limit)
  }

  handleLimit(limit) {
    this.limit = limit
    this.page = 1
    this.fetchImages()
  }

  handlePage(page) {
    this.page = page
    this.fetchImages()
  }

  fetchImages() {
    axios
      .get(
        `https://picsum.photos/v2/list?page=${this.page}&limit=${this.limit}`
      )
      .then((result) => {
        this.images = result.data
        this.render()
      })
  }
}
