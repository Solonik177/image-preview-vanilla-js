import '../scss/common'
import App from '../views/App'

const rootElement = document.getElementById('root')
const app = new App(rootElement)
app.render()
