export default class {
  render() {
    return `<div class="modal">
                <div class="modal-content">
                    <span class="close-modal">&times;</span>
                    <img src="#" id="imagePreview" alt="preview" />
                </div>
            </div>`
  }
}