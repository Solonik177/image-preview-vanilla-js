export default class {
  constructor(totalPages) {
    this.totalPages = totalPages
  }

  render(currentPage) {
    return this.totalPages > 0
      ? `
          <div class="row">
            <div class="col-sm-12 d-flex center">
              <ul class="d-flex pagination">
              ${Array(this.totalPages)
                .join(0)
                .split(0)
                .map((item, i) => {
                  return `<li class="page-item ${
                    i === currentPage - 1 ? 'active' : ''
                  }" data-page="${++i}">${i}</li>`
                })
                .join('')}  
              </ul>
            </div>
          </div>
        `
      : `<div class="row"><div class="d-flex center"><h3>No records</h3></div></div>`
  }
}
